package com.s_t_e.guineagram;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {

    //AW5
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_ref_layout);

        /*
         * AW5 - Main Activity
         */
//        Fragment fragment = MasterFragment.newInstance();
//        FragmentManager fragmentManager = getSupportFragmentManager();
//        fragmentManager.beginTransaction()
//                .replace(R.id.fragment_container, fragment, MasterFragment.TAG)
//                .commit();\

        /*
         * AW10 - Get the beers from the intent and send to Fragment
         */
        ArrayList<BrewDogBeer> beers = (ArrayList<BrewDogBeer>) getIntent().getSerializableExtra(SplashScreenActivity.ARG_BEERS);
        Fragment fragment = MasterFragment.newInstance(beers);
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.fragment_container, fragment, MasterFragment.TAG)
                .commit();
    }

    @NotNull
    public void showBeerPage(@NotNull BrewDogBeer beer) {
        Fragment nextFragment = DetailFragment.Companion.newInstance(beer);

//        getSupportFragmentManager().beginTransaction()
//                .replace(R.id.fragment_container, nextFragment, DetailFragment.TAG)
//                .addToBackStack(null)
//                .commit();

        /*
         * AW11 - Tablet
         */
        if (findViewById(R.id.detail_fragment_container) != null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.detail_fragment_container, nextFragment, DetailFragment.TAG)
                    .addToBackStack(null)
                    .commit();
            return;
        }
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, nextFragment, DetailFragment.TAG)
                .addToBackStack(null)
                .commit();
    }
}

