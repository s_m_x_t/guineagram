package com.s_t_e.guineagram

import android.util.Log
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException

//AW6 - Doing the network request
class BeerRepository(val onBeerRunCompleteCallback: BeerRepository.OnBeerRunCallComplete) {

    interface OnBeerRunCallComplete : OnCallComplete {
        fun onBeerRunResponse(beers: List<BrewDogBeer>?)
        fun onBeerRunFellThrough(error: ApiError)
    }

    private var publicApi: PublicApi

    init {
        val retrofit = Retrofit.Builder()
                .baseUrl("https://api.punkapi.com/v2/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        publicApi = retrofit.create(PublicApi::class.java)
    }

    fun fillMeUp() {
        var beers: List<BrewDogBeer>?
        val apiCall: Call<List<BrewDogBeer>> = publicApi.getBeers()
        apiCall.enqueue(object : Callback<List<BrewDogBeer>> {
            override fun onResponse(call: Call<List<BrewDogBeer>>, response: Response<List<BrewDogBeer>>) {
                if (response.isSuccessful) {
                    beers = response.body()
                    onBeerRunCompleteCallback.onBeerRunResponse(beers)
                    return
                }
                val apiError = ApiError(response.code(), response.message())
                onBeerRunCompleteCallback.onBeerRunFellThrough(apiError)
            }

            override fun onFailure(call: Call<List<BrewDogBeer>>, throwable: Throwable) {
                Log.e(BeerRepository::class.java.simpleName, throwable.toString())
                when (throwable) {
                    is IOException -> {
                        onBeerRunCompleteCallback.onTimeout()
                    }
                    else -> {
                        onBeerRunCompleteCallback.onUnknownError()
                    }
                }
            }
        })
    }

}
