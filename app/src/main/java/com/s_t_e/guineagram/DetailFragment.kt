package com.s_t_e.guineagram

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions

//AW 8
class DetailFragment: Fragment() {

    companion object {
        const val TAG = "MasterFragment"
        const val ARG_BEER = "arg_beer"

        fun newInstance(beer: BrewDogBeer) : DetailFragment {
            val fragment = DetailFragment()
            val args = Bundle()
            args.putSerializable(ARG_BEER, beer)
            fragment.arguments = args
            return fragment
        }
    }

    lateinit var beerImage : ImageView
    lateinit var abv: TextView
    lateinit var beerName: TextView
    lateinit var tagline: TextView
    lateinit var description: TextView
    lateinit var foodPairing: TextView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_detail, container, false)

        beerImage = view.findViewById(R.id.beer_image)
        abv = view.findViewById(R.id.abv)
        beerName = view.findViewById(R.id.beer_name)
        tagline = view.findViewById(R.id.tagline)
        description = view.findViewById(R.id.description)
        foodPairing = view.findViewById(R.id.food_pairing)

        val beer : BrewDogBeer = arguments?.getSerializable(ARG_BEER) as BrewDogBeer
        Glide.with(this)
                .load(beer.imageUrl)
                .apply(RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.RESOURCE))
                .into(beerImage)
        abv.text = "${beer.abv}%" //TODO - convert to string resource
        beerName.text = beer.name
        tagline.text = beer.tagline
        description.text = beer.description
        foodPairing.text = beer.foodPairing.toString()

        return view
    }

}