package com.s_t_e.guineagram

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class BrewDogBeer(val id: Int,
                       val name: String,
                       val tagline: String,
                       val description: String,
                       @SerializedName("image_url") val imageUrl: String, //omit SerializedName to begin with
                       val abv: String,
                       @SerializedName("food_pairing") val foodPairing: List<String>
) : Serializable