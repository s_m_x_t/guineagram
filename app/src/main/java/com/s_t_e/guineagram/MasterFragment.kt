package com.s_t_e.guineagram;

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import java.util.*

class MasterFragment : Fragment(),
//        BeerRepository.OnBeerRunCallComplete, // AW10 - Disable
        BeerListAdapter.BeerTappedListener {

    companion object {
        const val TAG = "MasterFragment"
        private const val ARG_BEERS = "arg_beers"

//        @JvmStatic
//        fun newInstance(): MasterFragment {
//            return MasterFragment()
//        }

        //AW10
        @JvmStatic
        fun newInstance(beers: ArrayList<BrewDogBeer>?): MasterFragment {
            val fragment = MasterFragment()
            val args = Bundle()
            args.putSerializable(ARG_BEERS, beers)
            fragment.arguments = args
            return fragment
        }
    }

    //    private TextView results;
    //    private ImageView beerDisplay;
    private lateinit var beerRecyclerView: RecyclerView
//    private val beerRepository = BeerRepository(this)
    private val adapter = BeerListAdapter()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        /*
         * Step 1 / 2 - Update the text view with results (see callback also)
         */

        //        results = findViewById(R.id.results);
        //        beerRepository.fillMeUp();

        /*
         * Step 3 - show the beer image with Glide (see callback also)
         */

        //        beerDisplay = findViewById(R.id.beer_display);
        //        beerRepository.fillMeUp();

        /*
         * Step 4, add an adapter
         */

        //        beerRecyclerView = findViewById(R.id.beer_cabinet);
        //        beerRecyclerView.setAdapter(adapter);
        //        beerRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        //        beerRepository.fillMeUp();

        /*
         * Step 5 - add item decoration
         */
//        val view = inflater.inflate(R.layout.fragment_master, container, false)
//        beerRecyclerView = view.findViewById(R.id.beer_cabinet)
//        beerRecyclerView.adapter = adapter
//        beerRecyclerView.layoutManager = LinearLayoutManager(context)
//        beerRecyclerView.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
//        beerRepository.fillMeUp()
//        return view

        /*
         * Step 9 - add beer listener
         */
        val view = inflater.inflate(R.layout.fragment_master, container, false)
        beerRecyclerView = view.findViewById(R.id.beer_cabinet)
        beerRecyclerView.adapter = adapter

        adapter.setBeerTappedListener(this)

        beerRecyclerView.layoutManager = LinearLayoutManager(context)
        beerRecyclerView.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
//        beerRepository.fillMeUp()

        /*
         AW10 - Optimisations get beers
         */
        val beers = arguments?.getSerializable(ARG_BEERS) as ArrayList<BrewDogBeer>
        adapter.updateItems(beers)

        return view
    }

//    override fun onBeerRunResponse(beers: List<BrewDogBeer>?) {
        /*
         * First run, print the data to a text view
         */
//        for (Beer beer: beers) {
//            results.append(beer.toString() + "\n");
//        }

        /*
         * Second run, print the URL only to a text view
         */
//        for (Beer beer: beers) {
//            results.append(beer.getImageUrl() + "\n");
//        }

        /*
         * Third run, one image only
         */
//        Glide.with(this).load(beers.get(10).getImageUrl()).into(beerDisplay);

        /*
         * Fourth run, use an adapter
         */
//        adapter.updateItems(beers as ArrayList<BrewDogBeer>)

        /*
         * AW10 - All commented out
         */
//    }

//    override fun onBeerRunFellThrough(error: ApiError) {
//        showToast("${error.statusCode} ${error.message}")
//    }
//
//    override fun onTimeout() {
//        showToast("Last orders")
//    }
//
//    override fun onUnknownError() {
//        showToast("Blind drunk")
//    }

    override fun beerTapped(beer: BrewDogBeer) {
        val mainActivity : MainActivity? = activity as MainActivity
        mainActivity?.showBeerPage(beer)
    }
//
//    private fun showToast(message: String) {
//        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
//    }
}