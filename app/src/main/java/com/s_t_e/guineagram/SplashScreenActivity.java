package com.s_t_e.guineagram;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

//AW3 - Setting up the Activity
public class SplashScreenActivity extends Activity
        implements BeerRepository.OnBeerRunCallComplete {

    public static final String ARG_BEERS = "arg_beers"; //AW10
    private BeerRepository beerRepository = new BeerRepository(this); //AW10

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

//        View container = findViewById(R.id.logo_and_title_container);

        //AW4 - Set location listener
//        container.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View v) {
//                showMainPage();
//                return true;
//            }
//        });

        // AW10 - Optimisations
        beerRepository.fillMeUp();

    }

    // BASIC TOAST MESSAGE
    private void showMessage() {
        Toast.makeText(this, R.string.toast_some_inspiring_message, Toast.LENGTH_SHORT).show();
    }

    // SHOW GUINEA MEMES PAGE
    private void showMainPage() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    // AW10 - Optimise by enabling the next four methods
    @Override
    public void onBeerRunResponse(@Nullable List<BrewDogBeer> beers) {
        ArrayList serializableBeers = (ArrayList<BrewDogBeer>)beers;
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(ARG_BEERS, serializableBeers);
        startActivity(intent);
    }

    @Override
    public void onBeerRunFellThrough(@NotNull ApiError error) {
        showToast("${error.statusCode} ${error.message}");
    }

    @Override
    public void onTimeout() {
        showToast("Last orders");
    }

    @Override
    public void onUnknownError() {
        showToast("Blind drunk");
    }

    private void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
