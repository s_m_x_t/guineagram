package com.s_t_e.guineagram

import retrofit2.Call
import retrofit2.http.GET

interface PublicApi {

    @GET("beers")
    fun getBeers(): Call<List<BrewDogBeer>>

}
