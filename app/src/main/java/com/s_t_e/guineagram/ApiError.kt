package com.s_t_e.guineagram

data class ApiError(val statusCode: Int, val message: String)
