package com.s_t_e.guineagram

interface OnCallComplete {
    fun onTimeout()
    fun onUnknownError()
}