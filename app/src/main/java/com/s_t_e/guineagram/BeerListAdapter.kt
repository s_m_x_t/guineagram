package com.s_t_e.guineagram

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions

class BeerListAdapter : RecyclerView.Adapter<BeerListAdapter.BeerViewHolder>() {

    private val beers = mutableListOf<BrewDogBeer>()

    /**
     * Add this later as part of step A/W 9
     */
    private lateinit var beerTappedListener: BeerTappedListener

    override fun getItemCount(): Int {
        return beers.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BeerViewHolder {
        val beerView = LayoutInflater.from(parent.context).inflate(R.layout.beer_item, parent, false)
        return BeerViewHolder(beerView)
    }

    override fun onBindViewHolder(holder: BeerViewHolder, position: Int) {
        Glide.with(holder.itemView.context)
                .load(beers[position].imageUrl)
                .apply(RequestOptions().diskCacheStrategy(DiskCacheStrategy.RESOURCE))
                .into(holder.image)

        holder.beerName.text = beers[position].name
        holder.beerStrength.text = "${beers[position].abv}%"
        holder.beerStrapline.text = beers[position].tagline

        /*
         * Add this bit as part of Step AW/9
         */
        holder.itemView.setOnClickListener {
            beerTappedListener.beerTapped(beers[position])
        }
    }

    class BeerViewHolder(beerView: View) : RecyclerView.ViewHolder(beerView) {

        var image : ImageView = beerView.findViewById(R.id.beer_image)
        var beerName : TextView = beerView.findViewById(R.id.beer_name)
        var beerStrength : TextView = beerView.findViewById(R.id.beer_strength)
        var beerStrapline : TextView = beerView.findViewById(R.id.beer_strapline)
    }

    fun updateItems(beers: ArrayList<BrewDogBeer>) {
        this.beers.clear()
        this.beers.addAll(beers)
        notifyDataSetChanged()
    }

    //AW-9
    fun setBeerTappedListener(beerTappedListener: BeerListAdapter.BeerTappedListener) {
        this.beerTappedListener = beerTappedListener
    }

    //AW-9
    interface BeerTappedListener {
        fun beerTapped(beer: BrewDogBeer)
    }

}
